class Shoot extends ObjectGraphical {
    constructor(positionX, positionY, rangeX, rangeY, width, height) {
        super(null, positionX, positionY, rangeX, rangeY, width, height);
    }

    draw(ctx) {
        ctx.save();
        ctx.fillStyle = "#ff6666";
        ctx.fillRect(this.positionX, this.positionY, this.width, this.height);
        ctx.restore();
    }

    move() {
        this.positionY -= this.rangeY;
    }

    getX() {
        return this.positionX;
    }

    getY() {
        return this.positionY;
    }

    getHeight() {
        return this.height;
    }

    getWidth() {
        return this.width;
    }
}