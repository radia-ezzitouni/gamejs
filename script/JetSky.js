class JetSky extends ObjectGraphical {
    constructor(imageSrc, positionX, positionY, width, height) {
        super(imageSrc, positionX, positionY, 0, 0, width, height);
    }

    draw(ctx) {
        super.draw(ctx);
    }

    getX() {
        return this.positionX;
    }

    getY() {
        return this.positionY;
    }

    getHeight() {
        return this.height;
    }

    getWidth() {
        return this.width;
    }
}