class Flotant extends ObjectGraphical {
    constructor(imageSrc, positionX, positionY, rangeX, rangeY, width, height) {
        super(imageSrc, positionX, positionY, rangeX, rangeY, width, height);
    }

    draw(ctx) {
        super.draw(ctx);
    }

    move() {
        this.positionY += this.rangeY;
    }

    getX() {
        return this.positionX;
    }

    getY() {
        return this.positionY;
    }

    getHeight() {
        return this.height;
    }

    getWidth() {
        return this.width;
    }
}