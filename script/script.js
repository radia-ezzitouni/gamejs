// fait par Ezzitouni Radia & Aayadi Elmehdi
// jeu du jetsky 
// version 1

window.onload = init;
var gamej;

function init() {
    gamej = new GameMain();
    gamej.init()
}

function GameMain() {
    
    let canvas = document.querySelector("#myCanvas");
    let ctx = canvas.getContext('2d');
    let width, height;

    //#region "Musique"
    let musicExplose = "./music/Explosion.mp3";
    let musicShoot = "./music/Shoot.mp3";
    let musicGameOver = "./music/GameOver.mp3";
    let musicPrincipale = "./music/Jeu.mp3";
    let musicMenu = "./music/MusicMenu.mp3";
    let musicTime = "./music/CoinTime.mp3";

    let audioShoot = new Audio(musicShoot);
    let audioExplose = new Audio(musicExplose);
    let audioBackgroundMenu = new Audio(musicMenu);
    let audioBackgroundGame = new Audio(musicPrincipale);
    let audioBackgroundGameOver = new Audio(musicGameOver);
    let audioTime = new Audio(musicTime);

    audioBackgroundMenu.loop = true;
    audioBackgroundGame.loop = true;
    audioBackgroundGameOver.loop = true;
    
    //#endregion

    //#region "Déclaration"
    let jetsky;
    let imageJetSky = "./images/Jetsky.png";
    let isAlive = true;
    let score = 0;
    let time_end = 60; // temps du jeu
    let initialPostionJetX = canvas.width / 2;
    let initialPostionJetY = canvas.height - 350;

    let diminutionScoreByEnnemisPassage = 10; // quand une bouée n'est plus explosé perte de point

    let bouees = [];
    let pieces = [];

    let tab_image_ennemi = ['./images/bouee1.png', './images/bouee2.png'];
    let imagePiece = "./images/Cointime.png";

    let timerEnnemis = 0;
    let timerPiece = 0;

    let frequenceApparitionEnnemis = 60;
    let frequenceApparitionPiece = 800;

    let bonusOnShootEnnemis = 50; // le temps d'explosion bouée parmet d'ajouter 50pt au score
    let bonusOnshootPiece = 25; // le temps d'avoir une piece permet d'ajouter 25s au temps

    let tabShoot = [];
    let gameMode = 0;
    let compteurMenu = 0;

    let img = new Image(); // image du début
    let imgJouerPress = new Image(); // image clignotante
    let imgJouer = new Image(); // image jouer
    let imgGameOver = new Image(); // image gameover

    let message_debut = "(P)ause    (H)elp    (S)on"; // message a affiché au debut
    let messageScoreDuringGame = "Score : "; //message durant le jeu
    let messageScoreGameOver = "Votre score est de "; // message a la fin du jeu

    let muet = false; // verification du son
    let debut_timer = false; // verification si le timer est déjà lancé
    //#endregion


    window.addEventListener('mousemove', onMouseUpdate, false);
    window.addEventListener('click', createShoot, false);

    window.addEventListener('keydown', (event) => {
        
        if (event.keyCode === 32) {            
            if (gameMode === 0) {
                gameMode = 1;
            } else if (gameMode === 2) {
                renitialiseGame();
            }            
        } else if (event.keyCode === 80) { //P            
            if (gameMode === 1) {
                // si on est encours de jeu on peut mettre le jeu en pause
                alert("Pause Jeu");                
            }
        } else if (event.keyCode === 72) { //H              
            if (gameMode != 1) {
                // le click sur pour voir le help hors du deroulement du jeu      
                alert("Help:\n Ici on trouve tous les insctructions du jeu");          
            }
        } else if (event.keyCode === 83) { //S
            // couper le son        
            if (muet === false) {                               
                stopSound(audioBackgroundGame);
                stopSound(audioBackgroundGameOver);
                stopSound(audioBackgroundMenu);
            } else if (muet === true) {
                // lancer du son une autre fois
                if (gameMode === 0) {
                    audioBackgroundMenu.play();
                } else if (gameMode === 1) {
                    audioBackgroundGame.play();
                } else if (gameMode === 2) {
                    audioBackgroundGameOver.play();
                }
            }

        }
    }
    );

    function init() {
        width = canvas.width;
        height = canvas.height;
        audioBackgroundMenu.play();
        initialiseJetAndPosition();
        animate();
    }

    function initialiseJetAndPosition() {
        let x = initialPostionJetX;
        let y = initialPostionJetY;
        jetsky = new JetSky(imageJetSky, x, y, 95, 120);
    }

    function animate() {
        // mode 0 debut du jeu
        // mode 1 encours du jeu
        // mode 2 gameover
        if (gameMode === 0) {
            displayMainMenu();
        }
        else if (gameMode === 1) {

            audioBackgroundGame.play();            
            stopSound(audioBackgroundGameOver);
            stopSound(audioBackgroundMenu);

            ctx.clearRect(0, 0, width, height);
            displayScore(messageScoreDuringGame, 10, 30);
            displayTime(700, 30);

            // si je jetsky est mort passer au gameover
            if (!isAlive) {
                gameMode = 2;
            }

            // timer apparition ennemis et piece 
            if (timerEnnemis === frequenceApparitionEnnemis) {
                createEnnemis(1); // 1 c'est crée qu'un ennemi 
                timerEnnemis = 0;
                frequenceApparitionEnnemis = (Math.floor(Math.random() * 40) + 10);
            }

            // la frequence d'apparition piece ne change pas
            if (timerPiece === frequenceApparitionPiece) {
                createpiece(1);
                timerPiece = 0;
            }

            jetsky.draw(ctx);
            drawEnnemis();
            drawPiece();
            drawShoot();
            drawShoot_piece();
            timerEnnemis++;
            timerPiece++;

            // si le timer n'est jamais lancer le faire
            if (debut_timer === false){
                setInterval(diminuer_time , 1000) // executer la fonction chaque 1s
                debut_timer = true;
            }

        }

        // si le temps est fini gameover
        if (time_end === 0) {
            gameMode = 2;
        }

        //game over
        if (gameMode === 2) {
            displayMenuGameOver();
        }

        // refaire l'animation function animate
        requestAnimationFrame(animate);
    }


    // // fonction a voir comment diminuer le temps encours du jeu
    function diminuer_time() {
        time_end--;
    }


    //le menu principale
    function displayMainMenu() {
        audioBackgroundMenu.play();

        stopSound(audioBackgroundGameOver);
        stopSound(audioBackgroundGame);

        ctx.fillRect(0, 0, width, height);
        imgJouerPress.src = "./images/JouerStart.png";
        imgJouer.src = "./images/Jouer.png";
        img.src = "./images/MenuPrincipale.png";
        ctx.drawImage(img, 0, 0, width, height);
        if (compteurMenu > 30) {
            ctx.drawImage(imgJouerPress, 0, 0, width, height);
            if (compteurMenu == 60) {
                compteurMenu = 0;
            }
        } else {
            ctx.drawImage(imgJouer, 0, 0, width, height);
        }
        compteurMenu++;
        afficherInfo(message_debut, 250, 430)
    }


    // message d'afficher le score soit en cours du jeu soit a la fin
    function displayScore(message, x, y) {
        ctx.save();
        ctx.font = "30px Arial";
        ctx.fillStyle = "black";
        ctx.fillText(message + score, x, y);
        ctx.restore();
    }

    // message d'affiche du temps
    function displayTime(x, y) {
        ctx.save();
        ctx.font = "30px Arial";
        ctx.fillStyle = "black";
        ctx.fillText("Time: " + time_end, x, y);
        ctx.restore();
    }

    // message du debut
    function afficherInfo(message, x, y) {
        ctx.save();
        ctx.font = "30px Arial";
        ctx.fillStyle = "White";
        ctx.fillText(message, x, y);
        ctx.restore();
    }


    // création d'ennemis soit bouée 1 ou 2
    function createEnnemis(n) {
        for (var i = 0; i < n; i++) {
            let img = tab_image_ennemi[Math.floor(Math.random() * tab_image_ennemi.length)];
            let x = width * Math.random();
            let y = 0;
            let w = 40 + 10 * Math.random();
            let h = 40 + 10 * Math.random();
            let vx = 0;
            let vy = 2 + 1 * Math.random();

            let ennemi = new Flotant(img, x, y, vx, vy, w, h);
            bouees.push(ennemi);
        }
    }

    // creation piece
    function createpiece(n) {
        for (var i = 0; i < n; i++) {
            let img = imagePiece;
            let x = width * Math.random();
            let y = 0;
            let w = 40 + 10 * Math.random();
            let h = 40 + 10 * Math.random();
            let vx = 0;
            let vy = 2 + 1 * Math.random();

            let p = new Flotant(img, x, y, vx, vy, w, h);
            pieces.push(p);
        }
    }

    // apparition d'ennemis en jeu
    function drawEnnemis() {
        for (i = 0; i < bouees.length; i++) {
            bouees[i].draw(ctx);
            bouees[i].move();

            // detection collision
            if (detectCollision(jetsky, bouees[i])) {
                isAlive = false;
            }

            // verification si l'ennemi est disparu afin de diminuer le score
            if (bouees[i].getY() > 650) {
                bouees.splice(i, 1);
                if (score != 0) {
                    score -= diminutionScoreByEnnemisPassage;
                }
            }
        }
    }

    // apparition piece
    function drawPiece() {
        for (i = 0; i < pieces.length; i++) {
            pieces[i].draw(ctx);
            pieces[i].move();
        }
    }


    function drawShoot() {
        for (var i = 0; i < tabShoot.length; i++) {
            tabShoot[i].draw(ctx);
            tabShoot[i].move();

            for (var j = 0; j < bouees.length; j++) {
                if (typeof tabShoot[i] !== "undefined") {
                    if (detectCollision(tabShoot[i], bouees[j])) {
                        bouees.splice(j, 1);
                        tabShoot.splice(i, 1);
                        playSound(audioExplose);
                        incrementScoreOnShootEnnemi();
                    }
                }
            }


            if (typeof tabShoot[i] !== "undefined") {
                if (tabShoot[i].getY() < 0) {
                    tabShoot.splice(i, 1);
                }
            }
        }
    }

    function drawShoot_piece() {
        for (var i = 0; i < tabShoot.length; i++) {
            tabShoot[i].draw(ctx);
            tabShoot[i].move();

            for (var j = 0; j < pieces.length; j++) {
                if (typeof tabShoot[i] !== "undefined") {
                    if (detectCollision(tabShoot[i], pieces[j])) {
                        pieces.splice(j, 1);
                        tabShoot.splice(i, 1);
                        playSound(audioTime);
                        incrementScoreOnShootPiece();
                    }
                }
            }

            if (typeof tabShoot[i] !== "undefined") {
                if (tabShoot[i].getY() < 0) {
                    tabShoot.splice(i, 1);
                }
            }
        }
    }

    // Menu game Over
    function displayMenuGameOver() {
        audioBackgroundGameOver.play();

        stopSound(audioBackgroundMenu);
        stopSound(audioBackgroundGame);

        ctx.fillStyle = "#3399FF";
        ctx.fillRect(0, 0, width, height);
        imgGameOver.src = "./images/GameOver.png";
        ctx.drawImage(imgGameOver, 0, 0, width, height);
        displayScore(messageScoreGameOver, height / 2, 500);
    }



    // function pour detection collision
    function detectCollision(target, enemy) {
        return ((target.getX() < enemy.getX() + enemy.getWidth()) &&
            (target.getX() + target.getWidth() > enemy.getX()) &&
            (target.getY() < enemy.getY() + enemy.getHeight()) &&
            (target.getHeight() + target.getY() > enemy.getY()));
    }


    function playSound(objectAudio) {
        objectAudio.play();
    }

    function stopSound(objectAudio){
        objectAudio.pause();
        objectAudio.currentTime = 0;        
    }

    function incrementScoreOnShootEnnemi() {
        score += bonusOnShootEnnemis;
    }

    function incrementScoreOnShootPiece() {
        time_end += bonusOnshootPiece;
    }

    //position of hero = position of mouse
    function onMouseUpdate(e) {
        if (e.pageX < width - jetsky.width && e.pageY < height - jetsky.height) {
            jetsky.positionX = e.pageX;
            jetsky.positionY = e.pageY;
        }
    }

    function createShoot() {
        if (gameMode === 1) {
            let x = jetsky.getX() + 47;
            let y = jetsky.getY();

            let weightShoot = 5;
            let heightShoot = 10;
            let rangeXShoot = 0;
            let rangeYShoot = 10;

            let shoot = new Shoot(x, y, rangeXShoot, rangeYShoot, weightShoot, heightShoot);
            tabShoot.push(shoot);
            playSound(audioShoot);
        }
    }

    // à zéro
    function renitialiseGame() {
        debut_timer = false;
        gameMode = 1;
        isAlive = true;
        tabShoot = [];
        bouees = [];
        pieces = [];
        score = 0;
        time_end = 60;
    }

    return {
        init: init
    }
}

