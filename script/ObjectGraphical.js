class ObjectGraphical {
    constructor(imageSrc, positionX, positionY, rangeX, rangeY, width, height) {
        this.imageSrc = imageSrc;
        this.positionX = positionX;
        this.positionY = positionY;
        this.rangeX = rangeX;
        this.rangeY = rangeY;
        this.width = width;
        this.height = height;
    }

    draw(ctx) {
        ctx.save();
        var img = new Image();
        img.src = this.imageSrc;
        ctx.drawImage(img, this.positionX, this.positionY, this.width, this.height);
        ctx.restore();
    }
}

